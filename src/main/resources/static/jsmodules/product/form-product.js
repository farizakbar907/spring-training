$(document).off("click", "#btn-save");
$(document).on("click", "#btn-save", function(e) {

    let ProductId = $("#form-product #ProductId").val();

    console.log("Isi Id Product : " + ProductId);

    if (ProductId === "" || ProductId === null) {
        console.log("onSave()");

        let dataBody = {
             name: $("#form-product #name").val(),
             price: $("#form-product #price").val(),
             quantity: $("#form-product #quantity").val(),
             sku: $("#form-product #sku").val(),
        }

        console.log(dataBody);

        $.when(dipAjax.post('/api/product/save', dataBody)).done(function(result) {
            if (result.statusCode === "201") {
                console.log(result);
                swal({
                    title: "Save Success!",
                    text: "Your data has been saved",
                    icon: "success",
                    timer: 3000,
                    button: false
                }).then(() => {
                    window.location = "/product/list";
                });
            } else {
                console.log(result);
                swal({
                    title: "Failed!",
                    text: "Failed to Save Data",
                    icon: "error",
                    button: "OK",
                    timer: 5000
                });
                return false;
            }
        });

    } else {
        console.log("onUpdate()");

        let dataBody = {
            id: $("#form-product #ProductId").val(),
            name: $("#form-product #name").val(),
            price: $("#form-product #price").val(),
            quantity: $("#form-product #quantity").val(),
            sku: $("#form-product #sku").val(),
        }

        console.log(dataBody);

        $.when(dipAjax.post('/api/product/update', dataBody)).done(function(result) {
            if (result.statusCode === "201") {
                console.log(result);
                swal({
                    title: "Update Success!",
                    text: "Your data has been updated",
                    icon: "success",
                    timer: 3000,
                    button: false
                }).then(() => {
                    window.location = "/product/list";
                });
            } else {
                console.log(result);
                swal({
                    title: "Failed!",
                    text: "Failed to Save Data",
                    icon: "error",
                    button: "OK",
                    timer: 5000
                });
                return false;
            }
        });

    }


});