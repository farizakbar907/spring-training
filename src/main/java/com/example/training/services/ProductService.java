package com.example.training.services;

import com.example.training.models.Employee;
import com.example.training.models.Product;
import com.example.training.repository.ProductRepository;
import com.example.training.viewmodel.ResponseSave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    ProductRepository repository;

    public List<Product> getAllProduct(Product dataParam)
    {
        return repository.listProduct(dataParam);
    }

    public ResponseSave saveProduct(Product dataForSave){
        return repository.insertProduct(dataForSave);
    }

    public ResponseSave updateProduct(Product dataForUpdate){
        return repository.updateProduct(dataForUpdate);
    }

    public ResponseSave deleteProduct(Product dataForDelete){
        return repository.deleteProduct(dataForDelete);
    }
}
