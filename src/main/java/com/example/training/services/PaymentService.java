package com.example.training.services;

import com.aspose.words.Document;
import com.aspose.words.MailMergeCleanupOptions;
import com.aspose.words.SaveFormat;
import com.example.training.models.Employee;
import com.example.training.models.Payment;
import com.example.training.repository.PaymentRepository;
import com.example.training.utilities.ResourceHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.io.ByteArrayInputStream;
import java.util.List;

@Service
public class PaymentService {

    @Autowired
    PaymentRepository repository;


    public List<Payment> getAllPayment(Payment dataParam) {
        return repository.listPayment(dataParam);
    }

    public ByteArrayInputStream documentPaymentStream(Payment dataParam) throws Exception {

        // Get Data Row / byID from Database
        Payment paymentObject = repository.listPayment(dataParam).get(0);

        // Setup Path folder for reading resource
        String parentPath = ResourceHelper.getResourcePath();
        String pathFile = ResourceHelper.getFilePathFromResource("surat-tagihan", ".docx");

        // Config/Init Document Aspose Library
        Document doc = new Document(pathFile);
        System.out.println(doc);

        doc.getMailMerge().setTrimWhitespaces(true);
        doc.getMailMerge().setCleanupOptions(MailMergeCleanupOptions.REMOVE_UNUSED_FIELDS | MailMergeCleanupOptions.REMOVE_CONTAINING_FIELDS
                | MailMergeCleanupOptions.REMOVE_EMPTY_PARAGRAPHS);

        String[] toMerge = {
                "NAME", "INVOICENUMBER", "AMOUNT", "STATUS", "CREATEDATE", "CREATEBY", "COMPANY"
        };

        doc.getMailMerge().execute(toMerge, new Object[]{
                paymentObject.getName() == null ? "-" : paymentObject.getName(),
                paymentObject.getInvoiceNumber() == null ? "-" : paymentObject.getInvoiceNumber(),
                paymentObject.getAmount() == null ? "-" : paymentObject.getAmount(),
                paymentObject.getStatus() == null ? "-" : paymentObject.getStatus(),
                paymentObject.getCreateBy() == null ? "-" : paymentObject.getCreateBy(),
                paymentObject.getCreateDate() == null ? "-" : paymentObject.getCreateDate(),
                "PT Daya Indosa Pratama"
        });

        String generatedFileName = "Surat_Tagihan_"+ paymentObject.getInvoiceNumber();
        String finalFileName = parentPath + "/" + generatedFileName;
        String tmpWordDoc = finalFileName + ".docx";
        String wordDoc = finalFileName + ".docx";
        String pdfDoc = finalFileName + ".pdf";

        doc.save(wordDoc);
        doc.save(tmpWordDoc);

        // Save As PDF from Doc
        doc.save(pdfDoc, SaveFormat.PDF);
        byte[] bFile = ResourceHelper.readBytesFromFile(pdfDoc);
        return new ByteArrayInputStream(bFile);

    }
}




