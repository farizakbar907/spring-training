package com.example.training.services;

import com.example.training.models.Employee;
import com.example.training.repository.EmployeeRepository;
import com.example.training.viewmodel.ResponseSave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    EmployeeRepository repository;

    public List<Employee> getAllEmployee(Employee dataParam)
    {
        return repository.listEmployee(dataParam);
    }

    public ResponseSave saveEmployee(Employee dataForSave){
        return repository.insertEmployee(dataForSave);
    }

    public ResponseSave updateEmployee(Employee dataForUpdate){
        return repository.updateEmployee(dataForUpdate);
    }

    public ResponseSave deleteEmployee(Employee dataForDelete){
        return repository.deleteEmployee(dataForDelete);
    }



    public Integer saveNewEmployee(Employee dataForSave){

        //Operation example
        String nameUppercase = dataForSave.getName().toUpperCase();
        dataForSave.setName(nameUppercase);

        return repository.insertNewEmployee(dataForSave);
    }



    public List<Employee> getEmployee(String idEmployee){

        return repository.findEmployeeById(idEmployee);
    }
}

