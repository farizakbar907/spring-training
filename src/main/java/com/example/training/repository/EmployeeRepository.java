package com.example.training.repository;


import com.example.training.models.Employee;
import com.example.training.utilities.InformationConstant;
import com.example.training.utilities.JdbcHelper;
import com.example.training.viewmodel.EmployeeAddress;
import com.example.training.viewmodel.ResponseSave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.lang.String;

@Repository
public class EmployeeRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;
    SimpleJdbcCall simpleJdbcCall;

    public List<Employee> listEmployee(Employee dataParam){

        // Calling Stored Procedure To Get All USer List
        JdbcHelper helper = new JdbcHelper();
        simpleJdbcCall = helper.useTemplate(this.jdbcTemplate)
                .spName("SP_EMPLOYEE_LIST")
                .mapTo(Employee.class)
                .outParameter(InformationConstant.REF_CURSOR_RECORDSET)
                .build();

        // Set Query Param For Stored Procedure Requirment
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("P_ID", dataParam.getId())
                .addValue("P_NAME", dataParam.getName());

        // Stored Procedure Execution
        Map<String, Object> resultSp = simpleJdbcCall.execute(parameterSource);

        // Get Result Value To Object
        List<Employee> userLists = (List<Employee>) resultSp.get(InformationConstant.REF_CURSOR_RECORDSET);
        System.out.println("Isi Employee");
        System.out.println(userLists.get(0).getName());
        return userLists;
    }

    public ResponseSave insertEmployee(Employee dataForSave) {

        // Calling Stored Procedure To Get All User List
        JdbcHelper helper = new JdbcHelper();
        simpleJdbcCall = helper.useTemplate(this.jdbcTemplate)
                .spName("SP_EMPLOYEE_INSERT")
                .mapTo(ResponseSave.class)
                .outParameter(InformationConstant.REF_CURSOR_RECORDSET)
                .build();

        // Set Query Param for Stored Procedure Requirement
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("P_NAME", dataForSave.getName())
                .addValue("P_EMAIL", dataForSave.getEmail())
                .addValue("P_PHONE", dataForSave.getPhone())
                .addValue("P_ADDRESS", dataForSave.getAddress())
                .addValue("P_CREATEBY", dataForSave.getCreateBy());

        Map<String, Object> resultSp = simpleJdbcCall.execute(parameterSource);

        // Get Result Value to Object
        List<ResponseSave> responseSave = (List<ResponseSave>) resultSp.get("p_recordset");

        return responseSave.get(0);
    }



    public List<Employee> findAllEmployee(){
        // Get Current
        Date date = new Date();

        // Instance & Set Value Object Employee
        Employee dataEmployee = new Employee();

        // Set Dummy Value Employee
        List<Employee> returnValue = new ArrayList<>();
        for(int i = 0; i < 10; i++){
            dataEmployee.setName("User " + i);
          //  dataEmployee.setId("UID " + i);
            dataEmployee.setPhone("Phone Number " + i);
            dataEmployee.setEmail("Mail Sample " + i);
            dataEmployee.setAddress("City " + i);

            returnValue.add(i, dataEmployee);
        }
        return returnValue;
    }

    public List<Employee> findEmployeeById(String idEmployee){
        return null;
    }

    public Integer insertEmployeeAddress(EmployeeAddress dataForSave){
        // Luas Bangunan
        // Warna Rumah
        // Jumlah Mobil
        // Ingin menggunakan model Employee tapi ditambah Atribute diatas yang tidak ada di model Employee
        // gunakan viewmodel

        dataForSave.setHouseColor("Merah");

        return null;
    }

    public Integer insertNewEmployee(Employee dataForSave){

        UUID uuid = UUID.randomUUID();
        dataForSave.setName("Nama");
        dataForSave.setPhone("Asd");
        dataForSave.setEmail("abc@gmail.com");
        dataForSave.setAddress("ABCDEF");

        return null;
    }

    public ResponseSave updateEmployee(Employee dataForUpdate){
        // Calling Stored Procedure To Get All User List
        JdbcHelper helper = new JdbcHelper();
        simpleJdbcCall = helper.useTemplate(this.jdbcTemplate)
                .spName("SP_EMPLOYEE_UPDATE")
                .mapTo(ResponseSave.class)
                .outParameter(InformationConstant.REF_CURSOR_RECORDSET)
                .build();

        // Set Query Param for Stored Procedure Requirement
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("P_ID", dataForUpdate.getId())
                .addValue("P_NAME", dataForUpdate.getName())
                .addValue("P_EMAIL", dataForUpdate.getEmail())
                .addValue("P_PHONE", dataForUpdate.getPhone())
                .addValue("P_ADDRESS", dataForUpdate.getAddress())
                .addValue("P_EDITBY", dataForUpdate.getCreateBy());

        Map<String, Object> resultSp = simpleJdbcCall.execute(parameterSource);

        // Get Result Value to Object
        List<ResponseSave> responseSave = (List<ResponseSave>) resultSp.get("p_recordset");

        return responseSave.get(0);
    }

    public ResponseSave deleteEmployee(Employee dataForDelete){
        // Calling Stored Procedure To Get All User List
        JdbcHelper helper = new JdbcHelper();
        simpleJdbcCall = helper.useTemplate(this.jdbcTemplate)
                .spName("SP_EMPLOYEE_DELETE")
                .mapTo(ResponseSave.class)
                .outParameter(InformationConstant.REF_CURSOR_RECORDSET)
                .build();

        // Set Query Param for Stored Procedure Requirement
        SqlParameterSource parameterSource = new MapSqlParameterSource()

                .addValue("P_ID", dataForDelete.getId());


        Map<String, Object> resultSp = simpleJdbcCall.execute(parameterSource);

        // Get Result Value to Object
        List<ResponseSave> responseSave = (List<ResponseSave>) resultSp.get("p_recordset");

        return responseSave.get(0);
    }

    public Integer deleteAllEmployee(){
        return null;
    }

  //  public Integer insertEmployee(Employee dataForSave){
       // return null;
  //  }
}

