package com.example.training.repository;

import com.example.training.models.Employee;
import com.example.training.models.Payment;
import com.example.training.utilities.InformationConstant;
import com.example.training.utilities.JdbcHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class PaymentRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;
    SimpleJdbcCall simpleJdbcCall;

    public List<Payment> listPayment(Payment dataParam){
        // Calling Stored Procedure To Get All User List
        JdbcHelper helper = new JdbcHelper();
        simpleJdbcCall = helper.useTemplate(this.jdbcTemplate)
                .spName("SP_PAYMENT_LIST")
                .mapTo(Payment.class)
                .outParameter(InformationConstant.REF_CURSOR_RECORDSET)
                .build();

        // Set Query Param For Stored Procedure Requirment
        SqlParameterSource parameterSource = new MapSqlParameterSource()
             //   .addValue("P_ID", dataParam.getId())
                .addValue("P_InvoiceNumber", dataParam.getInvoiceNumber())
                .addValue("P_NAME", dataParam.getName());

        // Stored Procedure Execution
        Map<String, Object> resultSp = simpleJdbcCall.execute(parameterSource);

        // Get Result Value To Object
        List<Payment> paymentLists = (List<Payment>) resultSp.get(InformationConstant.REF_CURSOR_RECORDSET);

        return paymentLists;
    }


}

