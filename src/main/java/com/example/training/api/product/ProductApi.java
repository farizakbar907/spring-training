package com.example.training.api.product;

import com.example.training.models.Employee;
import com.example.training.models.Product;
import com.example.training.services.EmployeeService;
import com.example.training.services.ProductService;
import com.example.training.viewmodel.AjaxResponseBody;
import com.example.training.viewmodel.ResponseSave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/product")
public class ProductApi {
    @Autowired
    ProductService productService;

    @GetMapping(value = ("/list"))
    public List<Product> listProductApi(){

        return productService.getAllProduct(new Product());
    }

    @PostMapping(value =  ("/save"))
    public AjaxResponseBody saveProduct (@RequestBody Product dataParam){

        AjaxResponseBody responseBody = new AjaxResponseBody();

        ResponseSave save = productService.saveProduct(dataParam);

        if (save.getErrorMsg().equalsIgnoreCase("-")){
            responseBody.setStatusCode("201");

        } else {
            responseBody.setStatusCode("500");
        }
        responseBody.setMessage(save.getErrorMsg());

        return responseBody;

    }
    @PostMapping(value =  ("/update"))
    public AjaxResponseBody updateProduct (@RequestBody Product dataParam) {

        AjaxResponseBody responseBody = new AjaxResponseBody();

        ResponseSave save = productService.updateProduct(dataParam);
        if (save.getErrorMsg().equalsIgnoreCase("-")) {
            responseBody.setStatusCode("201");

        } else {
            responseBody.setStatusCode("500");
        }
        responseBody.setMessage(save.getErrorMsg());

        return responseBody;
    }

    @PostMapping(value =  ("/delete"))
    public AjaxResponseBody deleteProduct (@RequestBody Product dataParam){

        AjaxResponseBody responseBody = new AjaxResponseBody();

        ResponseSave save = productService.deleteProduct(dataParam);
        if (save.getErrorMsg().equalsIgnoreCase("-")){
            responseBody.setStatusCode("201");

        } else {
            responseBody.setStatusCode("500");
        }
        responseBody.setMessage(save.getErrorMsg());

        return responseBody;
    }
}
