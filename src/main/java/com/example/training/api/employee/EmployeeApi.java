package com.example.training.api.employee;

import com.example.training.models.Employee;
import com.example.training.services.EmployeeService;
import com.example.training.viewmodel.AjaxResponseBody;
import com.example.training.viewmodel.ResponseSave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/employee")
public class EmployeeApi {

    @Autowired
    EmployeeService employeeService;

    @GetMapping(value = ("/list"))
    public List<Employee> lisEmployeeApi(){
        return employeeService.getAllEmployee(new Employee());
    }

    @PostMapping(value =  ("/save"))
    public AjaxResponseBody saveEmployee (@RequestBody Employee dataParam){

        AjaxResponseBody responseBody = new AjaxResponseBody();

        ResponseSave save = employeeService.saveEmployee(dataParam);

        if (save.getErrorMsg().equalsIgnoreCase("-")){
            responseBody.setStatusCode("201");

        } else {
            responseBody.setStatusCode("500");
        }
        responseBody.setMessage(save.getErrorMsg());

        return responseBody;

    }
    @PostMapping(value =  ("/update"))
    public AjaxResponseBody updateEmployee (@RequestBody Employee dataParam) {

        AjaxResponseBody responseBody = new AjaxResponseBody();

        ResponseSave save = employeeService.updateEmployee(dataParam);
        if (save.getErrorMsg().equalsIgnoreCase("-")) {
            responseBody.setStatusCode("201");

        } else {
            responseBody.setStatusCode("500");
        }
        responseBody.setMessage(save.getErrorMsg());

        return responseBody;
    }

        @PostMapping(value =  ("/delete"))
        public AjaxResponseBody deleteEmployee (@RequestBody Employee dataParam){

            AjaxResponseBody responseBody = new AjaxResponseBody();

            ResponseSave save = employeeService.deleteEmployee(dataParam);
            if (save.getErrorMsg().equalsIgnoreCase("-")){
                responseBody.setStatusCode("201");

            } else {
                responseBody.setStatusCode("500");
            }
            responseBody.setMessage(save.getErrorMsg());

            return responseBody;



    }
}
