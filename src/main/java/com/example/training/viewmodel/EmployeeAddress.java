package com.example.training.viewmodel;

import com.example.training.models.Employee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data

public class EmployeeAddress {

    public Employee dataEmployee;
    public Integer totalCar;
    public Integer area;
    public String houseColor;
}
