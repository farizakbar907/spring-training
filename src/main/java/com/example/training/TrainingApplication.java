package com.example.training;

import com.example.training.models.Employee;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainingApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainingApplication.class, args);

		// Instance Object
		Employee dataEmployee = new Employee();

		// Set Attribute Value
		dataEmployee.setName("Fariz Akbar Assidiqie");
		dataEmployee.setAddress("Bandung");
		dataEmployee.setEmail("farizakbar907@gmail.com");
	//	dataEmployee.setId("001");
		dataEmployee.setPhone("0895806610604");

		// Get Attribute Field Value
		String employeeName = dataEmployee.getName();
		String employeeAddress = dataEmployee.getAddress();
		String employeeEmail = dataEmployee.getEmail();
	//	String employeeId = dataEmployee.getId();
		String employeePhone = dataEmployee.getPhone();

		// Show Attribute Field Value
		System.out.println("Employee Information");
		System.out.println("Name : " + employeeName);
		System.out.println("Address : " + employeeAddress);
		System.out.println("Name : " + employeeEmail);
	//	System.out.println("Name : " + employeeId);
		System.out.println("Name : " + employeePhone);


	}

}
