package com.example.training.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Product {

    public Integer id;
    public String name;
    public BigDecimal price;
    public String quantity;
    public String sku;
    public Timestamp createDate;
    public String createBy;
}
