package com.example.training.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Employee {



    // Declare
    public Integer id;
    public String name;
    public String address;
    public String phone;
    public String email;
    public Timestamp createDate;
    public String createBy;


}
