package com.example.training.utilities;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonHelper {

    public static String toJsonString(Object o){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.serializeNulls();
        Gson gson = gsonBuilder.disableHtmlEscaping().create();

        return gson.toJson(o);
    }
}
