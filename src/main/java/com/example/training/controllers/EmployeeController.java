package com.example.training.controllers;

import com.example.training.models.Employee;
import com.example.training.services.EmployeeService;
import com.example.training.utilities.JsonHelper;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


import java.util.List;

@Controller
@SpringBootApplication
@RequestMapping(value = "/employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;



    @GetMapping(value = "/list")
    public String listEmployee(Model model){
        Employee employeeParam = new Employee();
        List<Employee> data = employeeService.getAllEmployee(employeeParam);
        System.out.println(data);
        model.addAttribute("dataEmployee", data);
        return "employee/list";

    }

    @GetMapping(value = "/form_employee")
    public String addEmployee(Model model){
       // String titleCard="Add Employee";
       //  model.addAttribute("username", "Fariz");
    //     model.addAttribute("titleCard", titleCard);
       //  model.addAttribute("title", "Employee");

        model.addAttribute("dataEmployee", new Employee() );
      //  model.addAttribute("dataEmployee",)
        return "employee/form_employee";
    }



    @GetMapping(value = "list-role")
    public String listRoleEmployee(Model model){

        String titlePage = "Employee Role";
        String userName ="Admin";

        model.addAttribute("title", titlePage );
        model.addAttribute("user", userName );

        return "employee/list-role";
    }

   // public ResponseEntity<?> listEmployee(){

     //   List<Employee> responseBody = employeeService.getListEmployee();
      //  return ResponseEntity.ok("responseBody");
   // }

//    @PostMapping(value = "save")
//    public ResponseEntity<?> saveEmployee() {
//        return ResponseEntity.ok("This is Page Save Employee");
//    }
//
//    @GetMapping(value = "delete")
//    public ResponseEntity<?> deleteEmployee(){
//        return ResponseEntity.ok("This Is page Delte Employee");
//    }

    @GetMapping(value = "/edit-employee")
    public String editEmployee(@RequestParam String idEmployee, Model model){

        String titleCard ="Edit Employee";

        // Set Parameter For Filtering
        Employee employeeParam = new Employee();
        employeeParam.setId(Integer.parseInt(idEmployee));

        // Get Data From Service
        Employee data = employeeService.getAllEmployee(employeeParam).get(0);

        // Set Model For View Attributes

        model.addAttribute("dataEmployee", data);
        model.addAttribute("titleCard", titleCard);
        model.addAttribute("username", "Fariz");

        return "employee/form_employee";
    }

    @GetMapping(value = "list-datatable")
    public String listEmployeeDt(Model model){

        Employee employeeParam = new Employee();

        List<Employee> data = employeeService.getAllEmployee(employeeParam);


        String titlePage = "Employee Role";
        String userName ="Admin";

        model.addAttribute("dataEmployee", JsonHelper.toJsonString(data));
        model.addAttribute("title", titlePage );
        model.addAttribute("user", userName );

        return "employee/list-datatable";
    }



    @PostMapping(value = "/delete-employee")
    public String deleteEmployee(@RequestParam String idEmployee, Model model) {

        String titleCard = "Delete Employee";

        // Set Parameter For Filtering
        Employee employeeParam = new Employee();
        employeeParam.setId(Integer.parseInt(idEmployee));

        // Get Data From Service
        Employee data = employeeService.getAllEmployee(employeeParam).get(0);

        // Set Model For View Attributes

        model.addAttribute("dataEmployee", data);
        model.addAttribute("titleCard", titleCard);
        model.addAttribute("username", "Fariz");

        return "employee/form_employee";
    }
}
