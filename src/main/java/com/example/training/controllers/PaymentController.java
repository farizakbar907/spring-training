package com.example.training.controllers;

import com.example.training.models.Employee;
import com.example.training.models.Payment;
import com.example.training.services.PaymentService;
import com.example.training.utilities.JsonHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.util.List;

@Controller
@RequestMapping(value = "/payment")
public class PaymentController {

    @Autowired
    PaymentService paymentService;


    @GetMapping(value = "/list")
    public String listPayment(Model model){
        Payment paymentParam = new Payment();
        List<Payment> data = paymentService.getAllPayment(paymentParam);
       // System.out.println(data);
        model.addAttribute("dataPayment", data);
        return "payment/list";



    }

    @GetMapping(value = "list-datatable")
    public String listPaymentDt(Model model) throws Exception{

        // Set Parameter for sample
        Payment paymentParam = new Payment();
     //   paymentParam.setId("1");

        // call method
        paymentService.documentPaymentStream(paymentParam);

        List<Payment> data = paymentService.getAllPayment(paymentParam);


        String titlePage = "Payment Role";
        String userName ="Admin";

        model.addAttribute("dataPayment", JsonHelper.toJsonString(data));
        model.addAttribute("title", titlePage );
        model.addAttribute("user", userName );

        return "payment/list-datatable";
    }

    @GetMapping(value = "/print-invoice/{invoiceNumber}")
    public ResponseEntity<?> printInvoice(@PathVariable String invoiceNumber) throws Exception{
        ByteArrayInputStream in = null;

        // Set Parameter for sample
        Payment paymentParam = new Payment();
        paymentParam.setInvoiceNumber(invoiceNumber);

     //   paymentParam.setId("1");
        in = paymentService.documentPaymentStream(paymentParam);

        // call method
      //  paymentService.documentPaymentStream(paymentParam);
        // Get Data From Service
        Payment data = paymentService.getAllPayment(paymentParam).get(0);


        HttpHeaders headers = new HttpHeaders();
        String headerValue = "attachment; filename=" + "Surat-Tagihan" + ".pdf";
        headers.add("Content-Disposition", headerValue);

        headers.setCacheControl("must-revalidate, post-check=0, pre-checked=0");
        headers.setContentType(MediaType.parseMediaType("application/pdf"));

        return ResponseEntity.ok()
                .headers(headers)
                .body(new InputStreamResource(in));

    }

//    @GetMapping(value = "/print-payment")
//    public String printPayment(@RequestParam String idPayment, Model model){
//
//        String titleCard ="Print Payment";
//
//        // Set Parameter For Filtering
//        Payment paymentParam = new Payment();
//        paymentParam.setId(Integer.parseInt(idPayment));
//
//        // Get Data From Service
//        Payment data = paymentService.getAllPayment(paymentParam).get(0);
//
//        // Set Model For View Attributes
//
//        model.addAttribute("dataEmployee", data);
//        model.addAttribute("titleCard", titleCard);
//        model.addAttribute("username", "Fariz");
//
//        return "employee/form_employee";
//    }
}
