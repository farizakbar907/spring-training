package com.example.training.controllers;


import com.example.training.utilities.InformationConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller

public class HomePageController {

    @RequestMapping(value = "/dashboard", method = {RequestMethod.GET,RequestMethod.POST})
    public String dashboard(Model model){
        String pageTitle ="dashboard" + InformationConstant.websiteTitle;
        
        model.addAttribute("username", "Fariz");
        model.addAttribute("title", pageTitle);
        return "dashboard";
    }
}
