package com.example.training.controllers;

import com.example.training.models.Employee;
import com.example.training.models.Product;
import com.example.training.services.EmployeeService;
import com.example.training.services.ProductService;
import com.example.training.utilities.InformationConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@SpringBootApplication
@RequestMapping(value = "/product")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping(value = "/list")
    public String listProduct(Model model){
        Product productParam = new Product();
        List<Product> data = productService.getAllProduct(productParam);
        System.out.println(data);
        model.addAttribute("dataProduct", data);
        return "product/list";

    }

    @GetMapping(value = "/add-product")
    public String addProduct(Model model){
        // String titleCard="Add Employee";
        //  model.addAttribute("username", "Fariz");
        //     model.addAttribute("titleCard", titleCard);
        //  model.addAttribute("title", "Employee");

        model.addAttribute("dataProduct", new Product());
        //  model.addAttribute("dataEmployee",)
        return "product/form-product";
    }

    @GetMapping(value = "/edit-product")
    public String editProduct(@RequestParam String idProduct, Model model){

        String titleCard ="Edit Product";

        // Set Parameter For Filtering
        Product productParam = new Product();
        productParam.setId(Integer.parseInt(idProduct));

        // Get Data From Service
        Product data = productService.getAllProduct(productParam).get(0);

        // Set Model For View Attributes

        model.addAttribute("dataProduct", data);
        model.addAttribute("titleCard", titleCard);
        model.addAttribute("username", "Fariz");

        return "product/form-product";
    }

}
